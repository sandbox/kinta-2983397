<?php

namespace Drupal\group_users_admin;

use Drupal\user\UserAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\group\GroupMembershipLoader;

/**
 * Defines the custom access control handler for the user entity type.
 */
class UserGroupAccessHandler extends UserAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    \Drupal::logger('user_group_access')->notice("DEPRECATED CODE, remove when sure starit");
    return;
    // First check if access is already allowed:
    $preaccess_result = parent::checkAccess($entity, $operation, $account);
    if ($preaccess_result->isAllowed()){
      return $preaccess_result;
    }
    // Custom override code.
    // #Get $account 's groups where user has the administer users.
    //
    // ##Step 1: Get $account 's groups.
    /*$membership_storage = \Drupal::entityTypeManager()->getStorage('group_membership');
    if ($groups = $memberhsip_storage->loadByProperties(['uid' => $account->id()])) {
      #kint($groups);
    }
    */

    // kint($operation);
    // kint($entity);
    // kint($account);
    $membership_service = \Drupal::service('group.membership_loader');
    // Loads all memberships for a user not filtered by specific role as we 
    // check later the correct permission.
    $admin_memberships = $membership_service->loadByUser($account);//, 'entitat-grup_admin');


    // ##Step 2: Get $entity's (user's) groups.
    // kint($date->loadByUser($entity));
    // ##Step 3: Iterate over all admin memberships to check if user is there.
    $grant_access = FALSE;
    foreach ($admin_memberships as $admin_membership){
      // Check if account has administer members\' accounts
      // It's superseeded by administer group permission...
      //TODO allow edit only users with role monitor, not greater.
      if ($admin_membership->getGroup()->hasPermission('administer members\' accounts', $account)){
        // If has the permission load the user to edit.
        if ($membership_service->load($admin_membership->getGroup(), $entity)){
          // NEEDED TO override delete permission in: 
          // core/modules/user/src/ProfileForm.php
          // to allow group admins to delete/create account in its realm.
          $grant_access = TRUE;
          break;
        }
      }
    }
    return AccessResult::allowedIf($grant_access);

    $storage = \Drupal::entityTypeManager()->getStorage('group');
    if ($groups = $storage->loadByProperties(['uid' => $account->id()])) {
      //$storage->delete($groups);
    }


    $access_result = parent::checkAccess($entity, $operation, $account);
    switch ($operation) {
      case 'update':
        break;
      case 'create':
        break;
      case 'delete':
        break;
    }
    return $group_permission;

  }

}
